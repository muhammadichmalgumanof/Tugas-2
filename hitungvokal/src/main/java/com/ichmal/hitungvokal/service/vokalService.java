package com.ichmal.hitungvokal.service;

import com.ichmal.hitungvokal.model.vokalModel;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class vokalService {
    public String[] vokal(vokalModel vModel){
        int x = 0;
        boolean cekA = false, cekI = false, cekU = false, cekE = false, cekO = false;
        String kalimat = vModel.getKalimat();
        String[] str = new String[5];
        String[] output = new String[2];
        StringBuilder out = null;

        for (int i = 0; i < kalimat.length(); i++) {
            if (kalimat.charAt(i) == '<'){
                kalimat = kalimat.replace("<", "x");
                kalimat = kalimat.replace(">", "x");
                output[1] = "!!!!! Kemungkinan mau melakukan XSS !!!!!";
            }
        }
        for (int i = 0; i < kalimat.length(); i++) {
            if (!cekA){
                if (kalimat.charAt(i) == 'a' || kalimat.charAt(i) == 'A') {
                    str[x] = "a";
                    x++;
                    cekA = true;
                }
            }
            if (!cekI){
                if (kalimat.charAt(i) == 'i' || kalimat.charAt(i) == 'I'){
                    str[x] = "i";
                    x++;
                    cekI = true;
                }
            }
            if(!cekU){
                if (kalimat.charAt(i) == 'u' || kalimat.charAt(i) == 'U'){
                    str[x] = "u";
                    x++;
                    cekU = true;
                }
            }
            if(!cekE){
                if (kalimat.charAt(i) == 'e' || kalimat.charAt(i) == 'E'){
                    str[x] = "e";
                    x++;
                    cekE = true;
                }
            }
            if(!cekO){
                if (kalimat.charAt(i) == 'o' || kalimat.charAt(i) == 'O'){
                    str[x] = "o";
                    x++;
                    cekO = true;
                }
            }
        }
        for (int i = 0; i < str.length; i++) {
            if (str[i] != null){
                if (i == 0){
                    out = new StringBuilder(str[i]);
                } else {
                    Objects.requireNonNull(out).append(" dan ").append(str[i]);
                }
            }
        }
        output[0] = '"'+kalimat+'"'+" = "+x+" yaitu "+Objects.requireNonNull(out).toString();
        return output;
    }
}
