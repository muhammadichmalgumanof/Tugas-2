package com.ichmal.hitungvokal.model;

public class vokalModel {
    private String kalimat;

    public vokalModel(){
    }

    public vokalModel(String kalimat) {
        this.kalimat = kalimat;
    }

    public String getKalimat() {
        return kalimat;
    }

    public void setKalimat(String kalimat) {
        this.kalimat = kalimat;
    }
}
