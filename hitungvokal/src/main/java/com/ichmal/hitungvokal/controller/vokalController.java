package com.ichmal.hitungvokal.controller;

import com.ichmal.hitungvokal.model.vokalModel;
import com.ichmal.hitungvokal.service.vokalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class vokalController {
    vokalModel vModel = new vokalModel();

    @Autowired
    private vokalService vService;

    @RequestMapping("/")
    public String getVokal(Model model){
        model.addAttribute("vModel", vModel);
        return "index";
    }

    @RequestMapping(value = "/", params = "cari", method = RequestMethod.POST)
    public String cari(@ModelAttribute("vModel") vokalModel vModel, Model model){
        model.addAttribute("hasil", vService.vokal(vModel));
        return "index";
    }
}
