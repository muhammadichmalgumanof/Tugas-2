package com.ichmal.hitungvokal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HitungvokalApplication {

    public static void main(String[] args) {
        SpringApplication.run(HitungvokalApplication.class, args);
    }

}
