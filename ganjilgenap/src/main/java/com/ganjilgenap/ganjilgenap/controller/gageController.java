package com.ganjilgenap.ganjilgenap.controller;

import com.ganjilgenap.ganjilgenap.model.gageModel;
import com.ganjilgenap.ganjilgenap.service.gageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class gageController {
    gageModel ggModel = new gageModel();

    @Autowired
    private gageService ggService;

    @RequestMapping("/")
    public String getGanjilGenap(Model model){
        model.addAttribute("ggModel", ggModel);
        return "index";
    }

    @RequestMapping(value = "/", params = "cari", method = RequestMethod.POST)
    public String cari(@ModelAttribute("ggModel") gageModel ggModel, Model model){
        model.addAttribute("hasil", ggService.gage(ggModel));
        return "index";
    }
}
