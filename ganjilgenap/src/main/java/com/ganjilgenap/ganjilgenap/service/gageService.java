package com.ganjilgenap.ganjilgenap.service;

import com.ganjilgenap.ganjilgenap.model.gageModel;
import org.springframework.stereotype.Service;

@Service
public class gageService {
    public String[] gage(gageModel ggModel){
        int x = 0;
        int awal = ggModel.getAwal();
        int akhir = ggModel.getAkhir();
        int c = akhir - awal;
        String[] output = new String[c + 1];
        if(awal <= akhir){
            for (int i = awal; i <= akhir; i++) {
                if (i % 2 == 0){
                    output[x] = "Angka " + i + " adalah genap";
                    x++;
                } else {
                    output[x] = "Angka " + i + " adalah ganjil";
                    x++;
                }
            }
        } else{
            output[x] = "Angka Kedua harus lebih besar dari angka Pertama";
            x++;
        }
        return output;
    }
}
