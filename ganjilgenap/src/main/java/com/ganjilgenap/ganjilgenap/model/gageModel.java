package com.ganjilgenap.ganjilgenap.model;

public class gageModel {
    private int awal, akhir;

    public gageModel(){

    }
    public gageModel(int awal, int akhir) {
        this.awal = awal;
        this.akhir = akhir;
    }

    public int getAwal() {
        return awal;
    }

    public void setAwal(int awal) {
        this.awal = awal;
    }

    public int getAkhir() {
        return akhir;
    }

    public void setAkhir(int akhir) {
        this.akhir = akhir;
    }
}
