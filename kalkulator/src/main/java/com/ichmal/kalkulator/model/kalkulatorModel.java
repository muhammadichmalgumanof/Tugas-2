package com.ichmal.kalkulator.model;

public class kalkulatorModel {
    private String operasi;
    private int a, b;

    public kalkulatorModel(){

    }

    public kalkulatorModel(String operasi, int a, int b) {
        this.operasi = operasi;
        this.a = a;
        this.b = b;
    }

    public String getOperasi() {
        return operasi;
    }

    public void setOperasi(String operasi) {
        this.operasi = operasi;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
