package com.ichmal.kalkulator.controller;

import com.ichmal.kalkulator.model.kalkulatorModel;
import com.ichmal.kalkulator.service.kalkulatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class kalkulatorController {
    kalkulatorModel kalkulatorModel = new kalkulatorModel();

    @Autowired
    private kalkulatorService service;

    @RequestMapping("/")
    public String getKalkulator(Model model){
        model.addAttribute("kalkulator", kalkulatorModel);
        return "index";
    }

    @RequestMapping(value = "/", params = "tambah", method = RequestMethod.POST)
    public String getTambah(@ModelAttribute("kalkulator") kalkulatorModel kalkulator, Model model){
        model.addAttribute("hasil", service.tambah(kalkulator));
        return "index";
    }

    @RequestMapping(value = "/", params = "kurang", method = RequestMethod.POST)
    public String getKurang(@ModelAttribute("kalkulator") kalkulatorModel kalkulator, Model model){
        model.addAttribute("hasil", service.kurang(kalkulator));
        return "index";
    }

    @RequestMapping(value = "/", params = "kali", method = RequestMethod.POST)
    public String getKali(@ModelAttribute("kalkulator") kalkulatorModel kalkulator, Model model){
        model.addAttribute("hasil", service.kali(kalkulator));
        return "index";
    }

    @RequestMapping(value = "/", params = "bagi", method = RequestMethod.POST)
    public String getBagi(@ModelAttribute("kalkulator") kalkulatorModel kalkulator, Model model){
        model.addAttribute("hasil", service.bagi(kalkulator));
        return "index";
    }
}
