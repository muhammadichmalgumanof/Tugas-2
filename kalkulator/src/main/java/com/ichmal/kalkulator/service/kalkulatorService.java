package com.ichmal.kalkulator.service;

import com.ichmal.kalkulator.model.kalkulatorModel;
import org.springframework.stereotype.Service;

@Service
public class kalkulatorService {
    public int tambah(kalkulatorModel kalkulator){
        return kalkulator.getA() + kalkulator.getB();
    }

    public int kurang(kalkulatorModel kalkulator){
        return kalkulator.getB() - kalkulator.getA();
    }

    public int kali(kalkulatorModel kalkulator){
        return kalkulator.getA() * kalkulator.getB();
    }

    public String bagi(kalkulatorModel kalkulator){
        String hasil;
        String a = String.valueOf(kalkulator.getA());
        String b = String.valueOf(kalkulator.getB());
        if (kalkulator.getB() == 0){
            hasil = "Tidak Bisa Dilakukan";
        } else{
            float x = Float.parseFloat(a) / Float.parseFloat(b);
            hasil = String.valueOf(x);
        }
        return hasil;
    }
}
